import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('search/facets/availability-facets', 'Integration | Component | search/facets/availability facets', {
  integration: true
});

test('it renders', function(assert) {
  assert.expect(2);

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{search/facets/availability-facets}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#search/facets/availability-facets}}
      template block text
    {{/search/facets/availability-facets}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
