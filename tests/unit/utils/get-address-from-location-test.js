import getAddressFromLocation from '../../../utils/get-address-from-location';
import { module, test } from 'qunit';

module('Unit | Utility | get address from location');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = getAddressFromLocation();
  assert.ok(result);
});
