import { salaryRangeFromValue } from '../../../helpers/salary-range-from-value';
import { module, test } from 'qunit';

module('Unit | Helper | salary range from value');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = salaryRangeFromValue(42);
  assert.ok(result);
});
