import { languageNameFromValue } from '../../../helpers/language-name-from-value';
import { module, test } from 'qunit';

module('Unit | Helper | language name from value');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = languageNameFromValue(42);
  assert.ok(result);
});
