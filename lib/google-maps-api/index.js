module.exports = {
  name: 'google-maps-api',

  contentFor: function(type, config) {
    if (('body' === type) && ('production' === config.environment)) {
      return '<script src="//maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBid20frYA7FfQEs3OHAm34KM7a6j-ECq0"></script>';
    } else if ('body' === type) {
      return '<script src="//maps.googleapis.com/maps/api/js?libraries=places"></script>';
    }
  },
 
  isDevelopingAddon: function() {
    return true;
  }
};
