/* jshint node: true */

module.exports = function(deployTarget) {
  var ENV = {
    build: {}
    // include other plugin configuration that applies to all deploy targets here
  };

  if (deployTarget === 'development') {
    ENV.build.environment = 'development';
    // configure other plugins for development deploy target here
  }

  if (deployTarget === 'staging') {
    ENV.build.environment = 'staging';
		
	  ENV.s3 = {
	    accessKeyId: 'AKIAJ4T7YEFHW3QTWWCA',
	    secretAccessKey: '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',
	    bucket: 'candidates-database-dev',
	    region: 'sa-east-1'
	  };

	  ENV.redis = {
	    host: 'localhost', //the redis host where your index.html is stored. (required)
	    port: '6379',
			allowOverwrite: true
	  };
  }

  if (deployTarget === 'production') {
    ENV.build.environment = 'production';
    // configure other plugins for production deploy target here
		
	  ENV.redis = {
	  	host: 'trampos.co',
	    port: '6379',
			allowOverwrite: true
	  };
		
	  ENV.s3 = {
	    accessKeyId: 'AKIAJ4T7YEFHW3QTWWCA',
	    secretAccessKey: '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',
	    bucket: 'candidates-database',
	    region: 'sa-east-1'
	  };

	  ENV.firebase = 'https://trampos.firebaseio.com/';
  }

  // Note: if you need to build some configuration asynchronously, you can return
  // a promise that resolves with the ENV object instead of returning the
  // ENV object synchronously.
  return ENV;
};