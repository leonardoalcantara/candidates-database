/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'candidates-database',
    environment: environment,
    baseURL: '/',
    locationType: 'hash',
		i18n: {
		  defaultLocale: 'pt-br'
		},
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
			apiPath: 'api/v3'
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };
	
	ENV.contentSecurityPolicy = {
	  'default-src': "'none'",
		'frame-src' : "'self' static.ak.facebook.com s-static.ak.facebook.com www.facebook.com *.firebaseapp.com",
	  'script-src': "'self' 'unsafe-inline' 'unsafe-eval' maps.gstatic.com maps.googleapis.com apis.google.com connect.facebook.net",
	  'font-src': "'self' fonts.gstatic.com s3-sa-east-1.amazonaws.com",
	  'connect-src': "'self' wss://*.firebaseio.com",
	  'img-src': "'self' data: s3-sa-east-1.amazonaws.com maps.gstatic.com maps.googleapis.com *.trampos.co csi.gstatic.com",
	  'style-src': "'self' 'unsafe-inline' fonts.googleapis.com",
	  'media-src': "'self'"
	}
	
  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.firebase = {
			apiKey: "AIzaSyA3OnGKWU2YOVYMNNlRQFG_-lhUAdGWaW4",
	    authDomain: "trampos-dev.firebaseapp.com",
	    databaseURL: "https://trampos-dev.firebaseio.com",
	    storageBucket: "trampos-dev.appspot.com",
	  }
  }
	
  if (environment === 'staging') {
    ENV.firebase = {
			apiKey: "AIzaSyA3OnGKWU2YOVYMNNlRQFG_-lhUAdGWaW4",
	    authDomain: "trampos-dev.firebaseapp.com",
	    databaseURL: "https://trampos-dev.firebaseio.com",
	    storageBucket: "trampos-dev.appspot.com",
	  }
  }
	
  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
    ENV.firebase = {
			apiKey: "AIzaSyCSC6eruEKWjMYkOuIIVPKiEyeKWy4YHZ4",
	    authDomain: "trampos.firebaseapp.com",
	    databaseURL: "https://trampos.firebaseio.com",
	    storageBucket: "firebase-trampos.appspot.com"
	  }
  }

  return ENV;
};
