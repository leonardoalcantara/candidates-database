/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var env = EmberApp.env();

var isProductionLikeBuild = ['production', 'staging'].indexOf(env) > -1;
var fingerprintPrepend = '';

if(env == 'production'){
	fingerprintPrepend = '//s3-sa-east-1.amazonaws.com/candidates-database/';
} else if(env == 'staging'){
	fingerprintPrepend = '//s3-sa-east-1.amazonaws.com/candidates-database-dev/';
}
module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
		fingerprint: {
			enabled: isProductionLikeBuild,
			prepend: fingerprintPrepend,
			extensions: ['js', 'css', 'png', 'jpg', 'gif', 'map', 'svg', 'eot', 'ttf', 'woff', 'woff2']
		},
		sourcemaps: {
			enabled: true,
			extensions: ['js']
		},
		'ember-cli-bootstrap-sassy': {
	    'glyphicons': true
	  }
  });

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

	app.import('bower_components/moment/moment.js');
	app.import('bower_components/moment/locale/pt-br.js');
  return app.toTree();
};
