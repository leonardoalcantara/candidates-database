export default {
  // "some.translation.key": "Text for some.translation.key",
  //
  // "a": {
  //   "nested": {
  //     "key": "Text for a.nested.key"
  //   }
  // },
  //
  // "key.with.interpolation": "Text with {{anInterpolation}}"
	
	found: {
		one: "encontrado",
		other: "encontrados"
	},
	
	genders: {
		M: "Masculino",
		F: "Feminino"
	},
	
	apply_statuses: {
		qualified: "Aprovado",
		pending: "Pendente",
		disqualified: "Fora do perfil"
	},
	
	field_of_works: {
		"social-media": "social media",
		ti: "Tecnologia da informação",
		marketing: "Marketing",
		criacao: "Criação",
		design: "Design",
		planejamento: "Planejamento",
		atendimento: "Atendimento",
		jornalismo: "Jornalismo",
		producao: "Produção",
		rtv: "Rádio e TV",
		outros: "Outros",
		midia: "Mídia",
		programacao: "Programação",
		comercial: "Comercial"
	},
	
	language_levels: {
		elementary: 					"básico",
		limited_working: 			"pré-intermediário",
		minimum_professional: "intermediário",
		full_professional: 		"avançado",
		native_or_bilingual: 	"nativo ou fluente"
	},
	
	salary_range: {
		from: "{{from}} ou mais",
		to: "até {{to}}",
		from_to: "de {{from}} até {{to}}"
	},

	salaries: {
		"[* TO 1000]": "até R$ 1.000,00",
		"[1001 TO 2000]": "de R$ 1.001,00 até R$ 1.000,00",
		"[2001 TO 3000]": "de R$ 2.001,00 até R$ 3.000,00",
		"[3001 TO 4000]": "de R$ 3.001,00 até R$ 4.000,00",
		"[4001 TO *]": "R$ 4.001,00 ou mais"
	},
	
	availabilities: {
		full_time: "Trabalho integral",
		part_time: "Meio período",
		freelance: "Freela",		
		internship: "Estágio",
		voluntary: "Trab. voluntário"
	},
	
	misc: {
		notes: {
			one: "{{count}} anotação",
			other: "{{count}} anotações"
		},
		candidates: {
			one: "{{count}} candidato",
			other: "{{count}} candidatos"
		}
	}
};
