import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.resource('search', { path: '/' }, function(){
    this.route('favorites', {path: '/favorites'});
    this.route('applies', {path: '/applies'});
    this.route('company_applies', {path: '/company_applies'});
  })
});

export default Router;
