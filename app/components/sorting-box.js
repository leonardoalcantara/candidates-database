import Ember from 'ember';

export default Ember.Component.extend({
  sortOptions: [
    Ember.Object.create({ text: "Relevância", value: "" }),
    Ember.Object.create({ text: "Nome (de A-Z)", value:"name_s asc" }),
    Ember.Object.create({ text: "Nome (de Z-A)", value:"name_s desc" })
  ],
  actions: {
    sort: function(){
      this.sendAction("sort");
    }
  }
});
