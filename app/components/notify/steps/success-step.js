import Ember from 'ember';
import pathFor from 'candidates-database/utils/path-for';

export default Ember.Component.extend({
	
	classNames: ["step-container"],
	
	actions: {
		close: function(){
			this.sendAction('close')
		}
	}
});
