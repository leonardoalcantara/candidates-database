import Ember from 'ember';
import DS from 'ember-data';
import pathFor from 'candidates-database/utils/path-for';

const AlertInfo = Ember.Object.extend({
	total_after_send: function(){
		return this.get('alert_credits.alerts_remaining') - this.get('alert_report.will_alert');
	}.property('alert_credits.alerts_remaining', 'alert_report.will_alert'),
	
	wont_alert: function(){
		return this.get('alert_report.already_applied') + this.get('alert_report.alert_optout') + this.get('alert_report.already_sent');
	}.property('alert_report.already_applied', 'alert_report.alert_optout', 'alert_report.already_sent')	
});

export default Ember.Component.extend({

	currentScope: Ember.inject.service('current-scope'),
	
	search: Ember.inject.service(),
	company: Ember.inject.service(),
	classNames: ["step-container"],
	opportunities: [],
	
	_loadInfo: function(){
		var endpoint = pathFor(`/candidates_databases/alerts/new`);
		var params = Ember.$.extend({  
	    company_id: this.get('company.id'),
		  scopes: [this.get('currentScope.scope')],
		  opportunities: this.get('opportunities').map(function(item){
		  	return item.id;
		  }),
	  }, this.get('search.searchParams'));
		var promise = new Ember.RSVP.Promise(function(resolve, reject){
			Ember.$.ajax({
				url: endpoint,
			  type: 'POST',
			  data: JSON.stringify(params),
			  contentType: 'application/json',
			  success: function(data){
			  	resolve(AlertInfo.create(data));
			  },
			  error: function(error){
					reject(error);
			  },
			});
		});
		var alertInfo = DS.PromiseObject.create({ promise: promise });
		this.set('alertInfo', alertInfo);
	}.on('init'),
	
	negativeTotal: function(){
		if(this.get('alertInfo.total_after_send') < 0){
			return Math.abs(this.get('alertInfo.total_after_send'));
		} else {
			return false;
		}
	}.property('alertInfo.total_after_send'),
	
	canSubmit: function(){
		return this.get('alertInfo.isFulfilled') && this.get('alertInfo.alert_report.will_alert') > 0;
	}.property('alertInfo.isFulfilled', 'alertInfo.alert_report.will_alert'),
	
	actions: {
		back: function(){
			this.sendAction("back");
		},
		submit: function(){
			this.sendAction("submit");
		},
		close: function(){
			this.sendAction('close');
		}
	}
});
