import Ember from 'ember';

const ProgressObject = Ember.Object.extend({

	total: 0,
	initTime: 0,
	initValue: null,
	
	value: Ember.computed('_value', {
    get: function(key) {
      return this.get('_value');
    },
    set: function(key, value) {
    	if(!this.get('initValue')){
    		this.set('initValue', value);
    	}
      this.set('_value', value);
      return value;
    }
  }),
	
	init: function(){
		this.set('initTime', Date.now());
		var self = this;
		var interval = setInterval(function(){
			self.set('currentTime', Date.now());
			if(self.get('value') >= self.get('total')){
				clearInterval(interval);
			}
		}, 1000);
	},

	percentageValue: function(){
		return this.get('value') > 0 ? (this.get('value') / this.get('total')) * 100 : 0;
	}.property('value', 'total'),

	estimatedTimeLeft: function(){
		var timeDelta 	= this.get('currentTime') - this.get('initTime');
		var valueDelta 	= this.get('value') - this.get('initValue');
		var remaining 	= this.get('total') - this.get('value');
		var speed 			= valueDelta / timeDelta;
		var estimate 		= remaining / speed;

		if(isNaN(estimate) || !isFinite(estimate)){
			return null;
		} else {
			return this._secondsToTime(estimate/1000);
		}

	}.property('value', 'total', 'initTime', 'initValue', 'currentTime'),

	_secondsToTime: function(secs){
    var minutes = Math.floor(secs / 60);
    var seconds = Math.ceil(secs % 60);
    return {
        "m": minutes.toString(),
        "s": seconds < 10 ? "0" + seconds : seconds.toString()
    };
	}

})

export default Ember.Component.extend({
	classNames: ["step-container"],
	company: Ember.inject.service(),
	firebase: Ember.inject.service(),
	scheduleAlertsArray: Ember.A([]),
  opportunitiesSorting: ['scheduling_alert:desc','id'],
	sortedOpportunities: Ember.computed.sort('opportunities.content', 'opportunitiesSorting'),
	
	setup: function(){
		var self = this;
		var ref = this.get('firebase');
		this.get('opportunities').then(function(opportunities){
			opportunities.filterBy('scheduling_alert').forEach(function(item){
				item.set('progress', ProgressObject.create());
				var path 	= `companies/company:${self.get('company.id')}/candidates-database/alerts/alert:${item.scheduling_alert_id}`;
				var schedulingAlertRef = ref.child(path);
				var callback = Ember.run.bind(self, self.onSchedulingAlertValue, item, schedulingAlertRef);
				schedulingAlertRef.on('value', callback);
				self.get('scheduleAlertsArray').addObject(schedulingAlertRef);
			});
			self.set('initTime', Date.now());
		});
	}.on('init'),

	onSchedulingAlertValue: function(item, schedulingAlertRef, snapshot){ 
		var progress = snapshot.val();
		item.set('progress.total', progress.total_sends);
		item.set('progress.value', progress.total_sent);
		if(progress.total_sent >= progress.total_sends){
			item.set('scheduling_alert', false);
			schedulingAlertRef.off('value');
		} else {
			item.set('scheduling_alert', true);
		}
	},

	canProceed: function(){
		return this.get('opportunities').filterBy('selected').length > 0;
	}.property('opportunities.@each.selected'),
	
	closeComponent: function(){
		this.get('scheduleAlertsArray').forEach(function(ref){
			ref.off('value');
		});
	}.on('willDestroyElement'),
	
	actions:{
		next: function(){
			this.sendAction('next');
		},
		close: function(){
			this.sendAction('close');
		}
	}

});
