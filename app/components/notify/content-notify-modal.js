import Ember from 'ember';
import DS from 'ember-data';
import pathFor from 'candidates-database/utils/path-for';

export default Ember.Component.extend({

	currentScope: Ember.inject.service('current-scope'),
	
	classNames: ['alert-modal-content'],
	classNameBindings: ["steps.2.current:sucess-step"],
	opportunities: [],
	search: Ember.inject.service(),
	company: Ember.inject.service(),
	currentStep: 0,

	selectedOpportunities: function(){
		return this.get('availableOpportunities').filterBy('selected');
	}.property('availableOpportunities.@each.selected'),
	
	notifyData: function(){
		return { query:this.get('search.query') }, { filterQuery: this.get('search.filterQuery') };
	},

	_loadOpportunities: function(){
		var endpoint = pathFor(`/candidates_databases/alerts/opportunities?company_id=${this.get('company.id')}`);
		var promise = new Ember.RSVP.Promise(function(resolve, reject){
			Ember.$.ajax({
				url: endpoint,
			  type: 'GET',
			  contentType: 'application/json',
			  success: function(data){
					resolve(Ember.A(data.map(function(obj){ 
						return Ember.Object.create(obj); 
					})));
			  },
			  error: function(error){ reject(error); }
			});
		});
		this.set('availableOpportunities', DS.PromiseArray.create({ promise: promise }));
	}.on('init'),
	
	_saveAlert: function(){
		var endpoint = pathFor(`/candidates_databases/alerts/`);
		var params = Ember.$.extend({  
	    company_id: this.get('company.id'),
		  scopes: [this.get('currentScope.scope')],
		  opportunities: this.get('selectedOpportunities').map(function(item){
		  	return item.id;
		  }),
	  }, this.get('search.searchParams'));
		var promise = new Ember.RSVP.Promise(function(resolve, reject){
			Ember.$.ajax({
				url: endpoint,
			  type: 'POST',
			  data: JSON.stringify(params),
			  contentType: 'application/json',
			  success: function(data){
					resolve(data);
			  },
			  error: function(error){ reject(error); }
			});
		});
		this.set('saveAlertRequest', DS.PromiseObject.create({ promise: promise }));
	},
	
		
	scrollTopOnStepChange: function(){
		var self = this;
		Ember.run.scheduleOnce('afterRender', function(){
			self.$().closest('.alert-modal').scrollTop(0);
		});
	}.observes('currentStep'),
	
	_willDestroy: function(){
		this.set('currentStep', 0);
	}.on('willDestroyElement'),
	
	actions: {
		nextStep: function(){
			this.incrementProperty('currentStep');
		},
		
		previousStep: function(){
			this.decrementProperty('currentStep');
		},
		
		closeModal: function(){
			this.sendAction('closeModal');
		},
		
		saveAlert: function(){
			this.incrementProperty('currentStep');
			this._saveAlert();
		}
		
	}
});