import Ember from 'ember';

export default Ember.Component.extend({
	search: Ember.inject.service(),
	company: Ember.inject.service(),
	setup: function(){
		var opportunities = this.get('company.data.opportunities').filter(function(opportunity){
			return opportunity.published == true;
		});
		this.set('opportunities', opportunities);
	}.on('init'),
	notifyData: function(){
		return { query:this.get('search.query') }, { filterQuery: this.get('search.filterQuery') };
	}
});
