import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'span',
	didInit: Ember.on('init', function(){
		var a = "R$ "+this.get('a')+",00";
		this.set('number', a);
	})
});