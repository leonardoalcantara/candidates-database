import Ember from 'ember';

export default Ember.Component.extend({
	
	tagName: 'span',
	
  // @param integer n: length of decimal
  // @param integer x: length of whole part
  // @param mixed   s: sections delimiter
  // @param mixed   c: decimal delimiter
	n: 2,
	x: 3,
	s: '.',
	c: ',',
	
	value: null,
	
	currencyValue: function(){
		var n = this.get('n'),
				x = this.get('x'),
				s = this.get('s'),
				c = this.get('c');
		
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = parseInt(this.get('value')).toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
	}.property('value')
});