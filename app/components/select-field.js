import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'select',
	classNames: ['select-field'],
	emptyText: "selecione uma opção",
	addEmptyValue: true,
	options: [],
	value: "",

	setup: function(){
		var self = this;
		this.$().on('change',function(){
      var idx = self.$('option:selected').val();
			var selectedOption = self.get('optionsArray').objectAt(idx);
      self.set('value', selectedOption.get('value'));
			self.sendAction("change");
		});
	}.on('didInsertElement'),

	_setSelected: function(){
		var selectedOption = this.get('optionsArray').findBy('value', this.get('value'));
		var idx = this.get('optionsArray').indexOf(selectedOption);

		if(idx < 0) this.set('value', this.get('optionsArray.firstObject.value'));

		Ember.run.scheduleOnce('afterRender', this, function(){
			this.$().val(idx > -1 ? idx : 0);
		})
	}.observes('value', 'optionsArray.[]').on('didInsertElement'),

	optionsArray: function(){
		if(this.get('addEmptyValue')){
			var emptyItem = Ember.Object.create({ text: this.get('emptyText'), value: "" });
			var opts = [emptyItem];
		} else {
			var opts = [];
		}
		Ember.$.each(this.get('options'), function(index, item){
			opts.push(Ember.Object.create(item));
		});
    return Ember.ArrayProxy.create({ content: Ember.A(opts) })
	}.property("options"),

	unbindAll: function(){
		this.$().off("change");
	}.on('willDestroyElement')
});
