import Ember from 'ember';

export default Ember.Component.extend({

	tagName: 'div',
	classNames: ['facet-list'],
	classNameBindings: ["hiddenFacets:show-hidden-facets"],
	hiddenFacets: false,
	visibleItems: 10,
	
	maxVisibleIndex: function(){
		return this.get('visibleItems') - 1;
	}.property('visibleItems'),

	showMoreLink: function(){
		return this.get('content.length') > this.get('visibleItems');
	}.property('content.length', 'visibleItems'),
	
	hiddenFacetsLength: function(){
		return this.get('content.length') - this.get('visibleItems');
	}.property('content.length', 'visibleItems'),
	
	actions: {
		showHiddenFacets: function(){
			this.set('hiddenFacets', true);
		},
		hideHiddenFacets: function(){
			this.set('hiddenFacets', false);
		},
		hideMore: function(content){
			this.set('showMore', false);
			// this.sendAction('hideMore', content);
		},
		viewMore: function(content){
			this.set('showMore', true);
			// this.sendAction('viewMore', content);
		}
	}
});
