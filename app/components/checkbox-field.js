import Ember from 'ember';

export default Ember.Component.extend({
	
	bubble: true,
	tagName: 'button',
	classNames: ['checkbox-field'],
	classNameBindings: ["checked:checked"],
	
	checked: false,
	
	click: function (event) {
    this.set('checked', !this.get('checked'))
		if(this.get('bubble') == false) event.stopPropagation();
		this.sendAction('action');
  }
});
