import Ember from 'ember';
import formatNumber from 'candidates-database/utils/format-number';

export default Ember.Component.extend({
	
	tagName: 'span',
	
	n: 2,
	s: '.',
	c: ',',
	
	value: null,
	
	currencyValue: function(){
		var n = this.get('n'),
				s = this.get('s'),
				c = this.get('c');

    return formatNumber(this.get('value'), n, s, c);
	}.property('value')
});