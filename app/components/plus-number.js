import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'span',
	didInit: Ember.on('init', function(){
		var a = this.get('a');
		var b = this.get('b');
		this.set('number', a + b);
	})
});