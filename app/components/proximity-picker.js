import Ember from 'ember';

export default Ember.Component.extend({

	proximitySelectedIndex: null,
	proximityValues: [1,2,3,4,5,6,7,8,9,10,20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,"sem limite"],

	proximity: Ember.computed('proximityValues', 'proximitySelectedIndex', {
    get: function(key) {
			let val = this.get('proximityValues')[this.get('proximitySelectedIndex')];
      return isNaN(val) ? null : val;
    },
    set: function(key, value) {
      if(value){
        this.set('proximitySelectedIndex', this.get('proximityValues').indexOf(value));
      }
      return value || [];
    }
  })
});
