import Ember from 'ember';

export default Ember.Component.extend({
	stars: [0, 1, 2, 3, 4]
});