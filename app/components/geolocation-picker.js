import Ember from 'ember';
import getAddressFromLocation from 'candidates-database/utils/get-address-from-location';

export default Ember.Component.extend({

	geolocation: Ember.inject.service(),
	company: Ember.inject.service(),
	
	classNames:["geolocation-picker"],
	map: null, // Will contain map object.
	marker: false, // Has the user plotted their location marker?

	ready: false,
	activating: false,
	picking: false,
	value: false,

	suggestions: Ember.A([]),
	noScrollBody: function(){
		if(this.get('picking')){
			Ember.$('html, body').addClass('noScroll');
		} else {
			Ember.$('html, body').removeClass('noScroll');
		}
	}.observes('picking'),
	
	loadGeolocation: function(){
		var self = this;
		self.get('geolocation').activate().finally(function(){
			if ( !(self.get('isDestroyed') || self.get('isDestroying')) ) {
				self.set('ready', true);
			}
		});
	}.on('init'),
	
	buildSuggestions: function(){
		var self = this;
		self.get('suggestions').clear();
		self.get('geolocation').activate().then(function(position){
			position.get('address').then(function(){
				let suggestion = Ember.Object.create({ 
					title: "Localização Atual", 
					address: position.get('address.content'), 
					position: { 
						lat: position.get('lat'), 
						lng: position.get('lng') 
					} 
				});
				self.get('suggestions').insertAt(0, suggestion);
			});			
		});
		
		Ember.$.each(self.get('company.addresses'), function(index, address){
			let suggestion = Ember.Object.create({ 
				title: address.title, 
				address: address.text, 
				position: { 
					lat: address.lat, 
					lng: address.lng 
				} 
			});
			self.get('suggestions').addObject(suggestion);
		});

		if(self.get('geolocation.position')){
			self.get('geolocation').activate().then(function(position){
				self.set('value', Ember.Object.create({
					lat: self.get('geolocation.position.lat'),
					lng: self.get('geolocation.position.lng')
				}));
			});
		} else {
			self.set('value', Ember.Object.create({
				lat: self.get('suggestions.0.position.lat'),
				lng: self.get('suggestions.0.position.lng')
			}));
		}

		self.sendAction('setUserGeo', self.get('value'));

	}.on('init'),
	
	isReady: function(){
		return this.get('ready');
	}.property('ready'),

	address: function(){
		return getAddressFromLocation(this.get('value.lat'), this.get('value.lng'));
	}.property('value.lat', 'value.lng'),

	buildMap: function() {
		var self = this;

		Ember.run.scheduleOnce('afterRender', this, function(){

			if(self.get('value')){
				// Já temos a localização, não faz nada.
			} else if(self.get('geolocation.position')){
				self.set('value', Ember.Object.create({
					lat: self.get('geolocation.position.lat'),
					lng: self.get('geolocation.position.lng')
				}));
			} else {
				self.set('value', Ember.Object.create({
					lat: self.get('suggestions.0.position.lat'),
					lng: self.get('suggestions.0.position.lng')
				}));
			}

	    //Map options.
	    var options = {
				streetViewControl: false,
				mapTypeControl: false,
				center: new google.maps.LatLng(self.get('value.lat'), self.get('value.lng')),
				draggable: false,
	      zoom: 15 //The zoom value.
	    };

	    // Create the map object.
	    self.set('map', new google.maps.Map(self.$('#geolocation-picker-map').get(0), options));

			google.maps.event.addListener(self.get('map'), 'idle', function () {
				self.set('value.lat', self.get('map').getCenter().lat());
				self.set('value.lng', self.get('map').getCenter().lng());
			});

			self.buildAutoComplete();
		});
	},

	buildAutoComplete: function(){

		var self = this;
		var service = new google.maps.places.AutocompleteService();

	  var displaySuggestions = function(predictions, status) {
	    if (status != google.maps.places.PlacesServiceStatus.OK) {
	      console.log(status);
	      return;
	    }
			self.set('predictions', Ember.A([]));
	    predictions.forEach(function(prediction) {
				self.get('predictions').pushObject({
					title: prediction.terms[0].value,
					text: prediction.terms.length > 1 ? prediction.description.substring(prediction.terms[1].offset) : "",
					placeId: prediction.place_id
				});
	    });
	  };

		var getPredictions = function(){
			service.getPlacePredictions({ input: self.$('#geolocation-picker-input-places').val() }, displaySuggestions);
		};

		self.$('#geolocation-picker-input-places').on('keyup input',  function(){
			Ember.run.debounce(self, getPredictions, 500);
		});
	},

	actions: {
		closeModal: function(){
			this.set('picking', false);
		},
		resetSearch: function(){
			this.set('predictions', Ember.A([]));
		},
		pick:function(){
			this.set('picking', true);
			this.buildMap();
		},

		defineLocation: function(){
			var coords = this.get('map').getCenter();
			this.set('picking', false);
		},

		selectPrediction: function(prediction){
			var self = this;
			var service = new google.maps.places.PlacesService(this.get('map'));
			var request = {
		    placeId: prediction.placeId
		  };
			this.get('predictions').clear();
			service.getDetails(request, function(place, status) {
		    if (status == google.maps.places.PlacesServiceStatus.OK) {
		    	self.get('map').setCenter(place.geometry.location);
		    }
		  });
		},
		
		selectSuggestion: function(suggestion){
			this.get('map').setCenter(new google.maps.LatLng(suggestion.get('position.lat'), suggestion.get('position.lng')));
		}
	}

});
