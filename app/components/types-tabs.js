import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ["types-tabs"],
	types: [Ember.Object.create({ text:"TODOS", 				value:"all", selected: true }),
					Ember.Object.create({ text:"PROFISSIONAL", 	value:"professional" }),
					Ember.Object.create({ text:"ESTAGIÁRIO", 		value:"internship" }),
					Ember.Object.create({ text:"FREELANCER", 		value:"freelance" })],
	
	actions: {
		changeType: function(type){
			this.get('types').setEach('selected', false);
			type.set('selected', true);
			this.sendAction('action', type.get('value'));
		}
	}
});
