import Ember from 'ember';

export default Ember.Component.extend({
	visiblePages: function(){
		var delta = 9;
		var idx = this.get('pages.current') - 1;
		var ini = idx - delta / 2 >= 0 ? idx - Math.floor(delta / 2) : 0;
		var end = ini + delta < this.get('pages.total') ? ini + delta : this.get('pages.total');
		var pages = [];
		for(var i = ini+1; i <= end; i++){
			pages.push({ number: i, current: this.get('pages.current') == i });
		}
		return pages;
	}.property('pages.total', 'pages.current'),

	actions: {
		nextPage: function(){
			this.sendAction('action', this.get('pages.current') + 1);
		},

		prevPage: function(){
			this.sendAction('action', this.get('pages.current') - 1);
		},

		gotoPage: function(page){
			this.sendAction('action', page);
		},
	}
});
