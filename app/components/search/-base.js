import Ember from 'ember';
import pathFor from 'candidates-database/utils/path-for';

export default Ember.Component.extend({

  search: Ember.inject.service(),
  company: Ember.inject.service(),

	stateValues: [
		{	text: "Todos", "value": "" },
		{	text: "São Paulo", value: 'SP' },
    {	text: "Rio de Janeiro",	value: 'RJ'	}],

	stateOptions: function(){
		return this.get('stateValues').map(function(item){ return Ember.Object.create(item) });
	}.property('stateValues.[]'),

	ageRangeValues: [	{ text: "Todas as idades",			value:"" },
										{ text: "menos de 18 anos", 		value:",18" },
										{ text: "de 19 a 24 anos", 			value:"19,24" },
				 						{ text: "de 25 a 34 anos", 			value:"25,34" },
										{ text: "de 35 a 44 anos", 			value:"35,44" },
										{ text: "de 45 a 54 anos", 			value:"45,54" },
										{ text: "de 55 a 64 anos", 			value:"55,64" },
				 						{ text: "65 ou mais", 		 			value:"65," }],

	ageRangeOptions: function(){
		return this.get('ageRangeValues').map(function(item){ return Ember.Object.create(item) });
	}.property('ageRangeValues.[]'),

 newerThanDaysValues: [{ text: "Todos", value:"" },
												 { text: "1 dia", value:"1" },
												 { text: "1 semana", value:"7" },
												 { text: "1 mês", value:"30" },
												 { text: "6 meses", value:"158" },
								 				 { text: "1 ano ou mais", value:"366" }],

	newerThanDaysOptions: function(){
		return this.get('newerThanDaysValues').map(function(item){ return Ember.Object.create(item) });
	}.property('newerThanDaysValues.[]'),

  actions: {
    search: function(){
      var self = this;
      self.set('nofilter', true);
      this.get('search').runSearch().then(function(response){
        self.set('results', response.results);
        self.set('facets', response.facets);
      });
    },

    filter: function(){
      var self = this;
      this.get('search').runFilter().then(function(results){
        self.set('results', results);
      	self.set('nofilter', false);
      });
    },

    sort: function(){
      var self = this;
      this.get('search').runSort().then(function(results){
        self.set('results', results);
      });
    },

    paginate: function(page){
      var self = this;
      this.get('search').changePage(page).then(function(results){
        self.set('results', results);
      });
    },

    paginateAndScrollTop: function(page){
      var self = this;
      this.get('search').changePage(page).then(function(results){
        self.set('results', results);
      });
      Ember.run.scheduleOnce("afterRender", this, function(){
        Ember.$("html, body").scrollTop(Ember.$(".section-content").offset().top);
      });
    },

		openApplyModal: function(applyId, candidate){
			var self = this;

			var applyPromise = new Ember.RSVP.Promise(function(resolve, reject){
				Ember.$.ajax({
				  url: pathFor(`/candidates_databases/applies?company_id=${self.get('company.id')}&apply_id=${applyId}`),
				  type: 'GET',
				  contentType: 'application/json',
				  success: function(data){
				  	resolve(data.apply);
				  },
				  error: function(error){
						reject(error);
				  }
				});
			});
			var model = {
				apply: DS.PromiseObject.create({ promise: applyPromise }),
				candidate: candidate
		  };
			this.sendAction('openModal', 'modals/apply-modal', model);
		},

		openCompanyModal: function(candidate){
			var self = this;
			var model = {
				candidate: candidate
		  };
		  
			this.sendAction('openModal', 'modals/company-modal', model);
		},

		openNotifyModal: function(candidate){
			var self = this;
			var model = {
				candidate: candidate
		  };
		  
			this.sendAction('openModal', 'modals/alert-modal', model);
		}
  }
});
