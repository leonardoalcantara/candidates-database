import Ember from 'ember';

export default Ember.Component.extend({

  recruiter: Ember.inject.service(),

	sortedOptions: Ember.computed.sort('options', function(a, b){
		if (a.isSelf) {
      return -1;
    } else if (b.isSelf){
    	return 1;
    } else {
    	if(a.name > b.name){
    		return 1;
    	} else {
    		return -1;
    	}
    }
    return 0;
	}),

  options: function(){
    if(!this.get('content')) return Ember.A([]);
    var self = this;
    let arr = this.get('content').map(function(item) {
      return Ember.Object.create({
        count: item.count,
        id: item.value.id,
        name: item.value.name,
        isSelf: item.value.id == self.get('recruiter.id'),
        selected: false
      });
    });
    return Ember.A(arr);
  }.property('content.[]'),

  value: Ember.computed('options.@each.selected', {
    get: function(key) {
      return this.get('options').filterBy('selected').map(function(option){
        return option.get('id');
      });
    },
    set: function(key, value) {
      var self = this;
      if(value){
        self.get('options').forEach(function(o){
          o.set('selected', value.indexOf(o.get('id')) > -1);
        });
      }
      return value || [];
    }
  }),

  actions: {
    change: function(){
      this.sendAction('change');
    }
  }
});
