import Ember from 'ember';

export default Ember.Component.extend({

  options: function(){
    if(!this.get('content')) return Ember.A([]);
    let arr = this.get('content').map(function(item) {
      let obj = Ember.Object.create(item);
      obj.set('selected', false);
      return obj;
    });
    return Ember.A(arr);
  }.property('content.[]'),

  value: Ember.computed('options.@each.selected', {
    get: function(key) {
      return this.get('options').filterBy('selected').map(function(option){
        return option.get('value');
      });
    },
    set: function(key, value) {
      var self = this;
      if(value){
        self.get('options').forEach(function(o){
          o.set('selected', value.indexOf(o.get('value')) > -1);
        });
      }
      return value || [];
    }
  }),

  actions: {
    change: function(){
      this.sendAction('change');
    }
  }
});
