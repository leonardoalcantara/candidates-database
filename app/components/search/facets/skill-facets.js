import Ember from 'ember';

export default Ember.Component.extend({

  optionsActive: function(){
    return this.get('options').filter(function(item, index){
      if(item.selected){
        return true;
      }
    });
  }.property('options.@each.selected'),

  optionsFiltered: function(){
    if(!this.get('optionsSearchValue')){
      return this.get('options').filter(function(item, index){
        if(!item.selected){
          return true;
        }
      });
    } else {

      var self = this;
      return this.get('options').filter(function(item, index){
        if(self.slugify(item.value).indexOf(self.slugify(self.get('optionsSearchValue'))) >= 0 && !item.selected){
          return true;
        }
      });

    }
  }.property('optionsSearchValue', 'options.@each.selected'),

  options: function(){
    if(!this.get('content')) return Ember.A([]);
    let arr = this.get('content').map(function(item) {
      let obj = Ember.Object.create(item);
      obj.set('selected', false);
      return obj;
    });
    return Ember.A(arr);
  }.property('content.[]'),

  value: Ember.computed('options.@each.selected', {
    get: function(key) {
      return this.get('options').filterBy('selected').map(function(option){
        return option.get('value');
      });
    },
    set: function(key, value) {
      var self = this;
      if(value){
        self.get('options').forEach(function(o){
          o.set('selected', value.indexOf(o.get('value')) > -1);
        });
      }
      return value || [];
    }
  }),

  slugify: function(text){
    return text.toString().toLowerCase()
      .replace(/ç/, 'c')
      .replace(/[àáâãäå]/,"a")
      .replace(/[èéêëẽ]/,"e")
      .replace(/[ìíîĩï]/,"i")
      .replace(/[òóõôö]/,"o")
      .replace(/[ùúûũü]/,"u")
      .replace(/\s+/g, '-')
      .replace(/\-\-+/g, '-')
  },

  actions: {
    change: function(){
      this.sendAction('change');
    }
  }
});
