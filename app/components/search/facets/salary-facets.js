import Ember from 'ember';

export default Ember.Component.extend({

  options: function(){
    if(!this.get('content')) return Ember.A([]);
    let arr = this.get('content').map(function(item) {
      let obj = Ember.Object.create(item);
      return obj;
    });
    return Ember.A(arr);
  }.property('content.[]'),

  value: Ember.computed('content.[]', 'salaryRangeValue', {
    get: function(key) {
      return [this.get('salaryRangeValue')];
    },
    set: function(key, value) {
      var self = this;
      if(value){
        self.set('salaryRangeValue', value);
      }
      return value || [];
    }
  }),

  actions: {
    change: function(){
      this.sendAction('change');
    }
  }
});
