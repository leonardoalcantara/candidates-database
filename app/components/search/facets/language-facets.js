import Ember from 'ember';

const LanguageFacetObject = Ember.Object.extend({

	selectedLevel: function(){
		return this.get('languageLevels')[this.get('selectedLevelIndex')];
	}.property('selectedLevelIndex', 'languageLevels'),

	selectedAt: function(){
		if(this.get('selected')){
			return $.now();
		} else {
			return null;
		}
	}.property('selected'),

	countForSelectedLevel: function(){
		var count = 0;
		for(var i = this.get('selectedLevelIndex'); i < this.get('languageLevels.length'); i++){
			count += this.get('languageLevels')[i].count;
		}
		return count;
	}.property('selectedLevelIndex'),

	count: function(){
		var count = 0;
		for(var i = 0; i < this.get('languageLevels.length'); i++){
			count += this.get('languageLevels')[i].count;
		}
		return count;
	}.property('languageLevels')
});

export default Ember.Component.extend({

  i18n: Ember.inject.service(),

	optionsSorting: ['selected:desc','selectedAt:asc','count:desc','languageName:asc'],
	sortedOptions: Ember.computed.sort('options', 'optionsSorting'),

  options: function(){
    if(!this.get('content')) return Ember.A([]);
    var self = this;
    let arr = this.get('content').map(function(item) {
      return LanguageFacetObject.create({
        languageName: item.value,
        languageLevels: item.levels,
        languageLevelsNames: item.levels.map(function(level){
          return self.get('i18n').t('language_levels.'+level.value);
        }),
        selected: false,
        selectedLevelIndex: 0
      });
    });
    return Ember.A(arr);
  }.property('content.[]'),

  value: Ember.computed('options.@each.selected', 'options.@each.selectedLevel', {
    get: function(key) {
      return this.get('options').filterBy('selected').map(function(option){
        return Ember.Object.create({
          name: option.get('languageName'),
          level: option.get('selectedLevel.value')
        });
      });
    },
    set: function(key, value) {
      var self = this;
      if(value){
        self.get('options').forEach(function(o){
          o.set('selected', value.isAny('name', o.get('languageName')));
        });
      }
      return value || [];
    }
  }),

  actions: {
    change: function(){
      this.sendAction('change');
    }
  }
});
