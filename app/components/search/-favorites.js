import Ember from 'ember';
import Base from 'candidates-database/components/search/-base';

export default Base.extend({
	company: Ember.inject.service(),
	currentScope: Ember.inject.service('current-scope'),
	
	printUrl: function(){
		return "//trampos.co/api/v3/candidates_databases/search/favorites.html?company_id="+this.get('company.id');
	}.property('company'),
  setup: function(){
  	this.get('currentScope').set('scope', 'favorites');
    this.set('search.endpoint', "/candidates_databases/search/favorites");
    this.get('search').clearFilters();
    this.get('search').clearResults();
		this.send('search');
  }.on('init'),

  actions: {
  	setUserGeo: function(value){
  		this.set('search.userGeo', value);
  	}
  }
});
