import Ember from 'ember';
import Base from 'candidates-database/components/search/-base';

export default Base.extend({
	currentScope: Ember.inject.service('current-scope'),
  setup: function(){
  	this.get('currentScope').set('scope', 'all');
    this.set('search.endpoint', "/candidates_databases/search");
    this.get('search').clearFilters();
    this.get('search').clearResults();
	this.send('search');
  }.on('init'),
  
  actions: {
  	setUserGeo: function(value){
  		this.set('search.userGeo', value);
  	}
  }
});
