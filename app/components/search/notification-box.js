import Ember from 'ember';

export default Ember.Component.extend({
	totalCredits: 0,
	usedCredits: 0,
	availableCredits: function(){
		return this.get('totalCredits') - this.get('usedCredits');
	}.property('totalCredits', 'usedCredits'),
	
	offsetDebit: function(){
		return this.get('selectedCount') - this.get('availableCredits');
	}.property('selectedCount', 'availableCredits'),

	actions: {
		openNotifyModal: function(){
			this.sendAction('openNotifyModal');
		}
	}
});
