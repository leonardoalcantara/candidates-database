import Ember from 'ember';

export default Ember.Component.extend({
	
	classNames: ["bar-chart"],
	total: 0,
	count: 0,
	
	delta: function(){
		return this.get('count') / this.get('total');
	}.property('total', 'count'),
	
	barStyle: function(){
		return `width:${Math.round(this.get('delta') * 100)}%`;
	}.property('delta')
});
