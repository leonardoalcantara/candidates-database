import Ember from 'ember';
import pathFor from 'candidates-database/utils/path-for';

export default Ember.Component.extend({
	company: Ember.inject.service(),
	recruiter: Ember.inject.service(),
	bubble: true,
	tagName: 'div',
	classNames: ['card-candidate', 'col-xs-12'],
	classNameBindings: ["candidate.favorited:favorite"],

	favoriteFiltered: function(){
		var self 					= this;
		var array 				= Ember.A([]);
		var favoritedBy 	= Ember.A(this.get('candidate.favorited_by'));

		if(this.get('candidate.favorited')){
			array.addObject(Ember.Object.create({ id: this.get('recruiter.id'), name: "mim" }));
		}

		$.each(favoritedBy, function(idx, recruiter){
			if(recruiter.id !== self.get('recruiter.id')){
				array.addObject(Ember.Object.create(recruiter));
			}
		});

		return array;
	}.property('candidate.favorited_by.[]', 'candidate.favorited'),

	favoritedPhrase: function(){
		var names = [];
		var phrase = "Favoritado por ";

		names = this.get('favoriteFiltered').map(function(recruiter){
			return "<strong>"+recruiter.name+"</strong>";
		});

		phrase += names.join(", ");

		if(names.get('length') >= 2){
			return phrase.replace(/,([^,]+)$/, ' e $1');
		} else if(names.get('length') === 1) {
			return phrase;
		} else {
			return "";
		}

	}.property('favoriteFiltered.[]'),

	sources: function(){
		let sources = [];

		if(this.get('candidate.sources.company_apply')){
			sources.push({
				type:'company_apply',
				data: this.get('candidate.sources.company_apply')
			});
		}

		if(this.get('candidate.sources.applies')){
			Ember.$.each(this.get('candidate.sources.applies'), function(index, item){
				sources.push({
					type:'apply',
					data: item
				});
			});
		}

		return sources;
	}.property('candidate.sources.company_apply', 'candidate.sources.applies.[]'),

	actions: {

		viewMore: function(data){
			data.set('showMore', true);
			// this.sendAction('viewMore', data);
		},

		hideMore: function(data){
			data.set('showMore', false);
			// this.sendAction('hideMore', data);
		},

		openApplyModal: function(id){
			this.sendAction('openApplyModal', id, this.get('candidate'));
		},

		closeCandidate: function(){
			this.sendAction('closeCandidate');
		},

		openCompanyModal: function(){
			console.log("Card");
			this.sendAction('openCompanyModal', this.get('candidate'));
		},

		closeCompanyModal: function(){
			this.sendAction('closeCompanyModal');
		},

		addFavorite: function() {
			var self = this;
			Ember.$.ajax({
			  url: pathFor('/candidates_databases/favorites'),
			  type: 'POST',
				contentType: 'application/json',
				data: JSON.stringify({ 
					company_id: self.get('company.id'),
					favorite: { 
						candidate_id: self.get('candidate.id'),
					} 
				}),
				success: function(){
					self.set('candidate.favorited', true);
					if(self.get('bubble') === false){ event.stopPropagation(); }
					self.sendAction('addFavorite', self.get('candidate'));
				},
				error: function(data){
					console.log(data.responseJSON.error);
				}
			});
		},

		removeFavorite: function() {
			var self = this;
			Ember.$.ajax({
			  url: pathFor('/candidates_databases/favorites'),
			  type: 'DELETE',
				contentType: 'application/json',
				data: JSON.stringify({ 
					company_id: self.get('company.id'),
					favorite: { 
						candidate_id: self.get('candidate.id'), 
					} 
				}),
				success: function(data){
					self.set('candidate.favorited', false);
					if(self.get('bubble') === false){ event.stopPropagation(); }
					self.sendAction('removeFavorite', self.get('candidate'));
				},
				error: function(data){
					console.log(data.responseJSON.error);
				}
			});
		}
	}
});
