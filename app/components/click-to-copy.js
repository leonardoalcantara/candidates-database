import Ember from 'ember';

export default Ember.Component.extend({
	tagName: "a",
	attributeBindings: ["title:title"],
	
	click:function(){
		var temp = Ember.$("<input>");
    Ember.$("body").append(temp);
    temp.val(this.$().text()).select();
    document.execCommand("copy");
    temp.remove();
	}
});
