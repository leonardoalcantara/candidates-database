import Ember from 'ember';

export default Ember.Component.extend({
	
	classNames: ["language-level"],
	levelOptions: [],
	selected: null,
  selectedIndex: 0,
	offsetLeft: 15,
	offsetRight: 20,
	
  setup: function() {
		this.set('currentIndex', this.get('selectedIndex'));
		this.$().on('mousedown', Ember.run.bind(this, this.startDrag));
  }.on('didInsertElement'),
	
	renderLevelDots: function(){
		var self = this;
		
		Ember.run.scheduleOnce('afterRender', this, function(){
			let len 			= self.get('levelOptions.length');
			let segments 	= (Math.pow(len, 2)-len)/len;
			let delta 		= (this.$().width() - this.get('offsetLeft') - this.get('offsetRight')) / (segments || 1);
			
			Ember.$.each(this.$('.level'), function(index, item){
				Ember.$(item).css({ left: `${self.get('offsetLeft') + delta * index}px` });
			});
			
			this.renderLevelOverlay();
		});
	}.observes('levelOptions.[]').on('didInsertElement'),
	
	renderLevelOverlay: function(){
		Ember.run.scheduleOnce('afterRender', this, function(){
			this.$('.level-overlay').css({ left: this.$('.level.selected').css('left') });
		});
	}.observes('selectedIndex'),
	
	startDrag: function(e){
		if(this.get('levelOptions.length') > 1){
			this.setRating(e);
			Ember.$(document).on('mousemove.starRating', Ember.run.bind(this, this.setRating));
			Ember.$(document).on('mouseup.starRating', Ember.run.bind(this, this.stopDrag));
		}
	},
	
	stopDrag: function(e){
		Ember.$(document).off('mousemove.starRating');
		Ember.$(document).off('mouseup.starRating');
		
		if(this.get('currentIndex') != this.get('selectedIndex')){
			this.set('currentIndex', this.get('selectedIndex'));
			this.sendAction('changed');
		}
	},
	
	setRating: function(e) {
		var offset 		= this.get('offsetLeft');
		var pos 			= e.pageX - this.$().offset().left - offset;
		var maxIndex 	= this.get('levelOptions.length') - 1;
		
		if(pos <= 0){
			var selectedIndex = 0;
		} else if(pos > this.$().width()){
			var selectedIndex = maxIndex;
		} else {
			var selectedIndex = Math.round(pos / (this.$().width() / (maxIndex || 1)));
		}
		
		this.set('selectedIndex', selectedIndex);
  }
});
