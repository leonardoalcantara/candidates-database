import Ember from 'ember';
import formatNumber from 'candidates-database/utils/format-number';

export default Ember.Component.extend({
	ranges: [],
	value: null,
	startRangeValue: "*",
	endRangeValue: "*",

	startRangeOptions: function(){
		var regex = /\[([^\s]+) TO [^\s]+\]/;
		return this.get('ranges').map(function(range){
			var match = range.value.match(regex);
			if(match[1] !== "*"){
				return Ember.Object.create({ value: match[1], text: formatNumber(match[1], 0, 3, '.', '')});
			} else {
				return Ember.Object.create({ value: match[1], text: "0"});
			}
		});
	}.property('ranges'),

	endRangeOptions: function(){
		var regex = /\[[^\s]+ TO ([^\s]+)\]/;
		var self = this;

		var content = Ember.A();

		this.get('ranges').map(function(range){

			var match = range.value.match(regex);

			if(match[1] !== "*"){
				var col1 = '';
				var toCompare = '';

				if(self.get('startRangeValue')){
					col1 = self.get('startRangeOptions').filterBy('value', self.get('startRangeValue'))[0];
				} else {
					col1 = self.get('startRangeOptions')[0];
				}

				if(col1.text !== "*"){
					toCompare = parseFloat(col1.text.replace(/\./g, ''));
				} else {
					toCompare = 0;
				}

				var parsing = parseFloat(match[1]);
				if(parsing > toCompare){
					content.addObject(Ember.Object.create({ value: match[1], text: formatNumber(match[1], 0, 3, '.', '') }));
				}
			} else {
				content.addObject(Ember.Object.create({ value: match[1], text: "Sem limite" }));
			}
		});

		return content;

	}.property('ranges', 'startRangeValue'),

	didChange: Ember.observer('startRangeValue', 'endRangeValue', function(){
		this.set('salaryRangeValue', `[${this.get('startRangeValue')} TO ${this.get('endRangeValue')}]`);
		Ember.run.debounce(this, this.sendAction, 'change', 1);
	})

});
