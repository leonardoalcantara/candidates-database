import Ember from 'ember';

export default Ember.Object.extend({
  toParam: function(){
    var serialize = function(obj){
      var serializedObj = {};
  		Object.keys(obj).forEach(function(key){
        if (obj[key] !== null && obj[key] !== undefined && obj[key] !== "") {
  			  serializedObj[Ember.String.underscore(key)] = (obj[key] instanceof Ember.Object) ? serialize(obj[key]) : obj[key];
        }
  		});
      return serializedObj;
    }
		return serialize(this);
  }
});