import Ember from "ember";

export default function getAddressFromLocation(lat, lng) {
	var geocoder = new google.maps.Geocoder();

	var promise = new Ember.RSVP.Promise(function(resolve, reject){
		geocoder.geocode({
			'latLng': new google.maps.LatLng(lat, lng)
		}, function (results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					resolve(results[0].formatted_address);
				} else {
					reject('No results found');
				}
			} else {
				reject('Geocoder failed due to: ' + status);
			}
		});
	});

	return DS.PromiseObject.create({
    promise: promise
  });
}
