import ENV from 'candidates-database/config/environment';

export default function pathFor(path) {
  return '/' + ENV.APP.apiPath + path
}
