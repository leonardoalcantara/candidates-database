// @param integer n: length of decimal
// @param integer x: length of whole part
// @param mixed   s: sections delimiter
// @param mixed   c: decimal delimiter

export default function formatNumber(value, n, s, c) {
	var regexp1 = new RegExp('(\\d)(?=(\\d{3})+\\.)', 'g');
	var regexp2 = new RegExp('\\.(\\d{' + n + '})$');
	return parseFloat(value).toFixed(n).replace(regexp1, "$1" + (s || ',')).replace(regexp2, (c || ',') + "$1");
}