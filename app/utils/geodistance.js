export default function geodistance(startPosition, endPosition) {
  var R = 6371e3; // metres
  var φ1 = startPosition.lat.toRadians();
  var φ2 = endPosition.lat.toRadians();
  var Δφ = (endPosition.lat-startPosition.lat).toRadians();
  var Δλ = (endPositionlat.lon-startPosition.lon).toRadians();

  var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
          Math.cos(φ1) * Math.cos(φ2) *
          Math.sin(Δλ/2) * Math.sin(Δλ/2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  var d = R * c;
}
