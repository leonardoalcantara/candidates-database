import Ember from 'ember';
import getAddressFromLocation from 'candidates-database/utils/get-address-from-location';

export default Ember.Service.extend({

	error: null,
	position: null,

	isSupported: function(){
		if(navigator.geolocation) {
			return true;
	  } else {
	    return false;
	  }
	}.property().volatile(),

	hasErrors: function(){
		return this.get('error') !== null;
	}.property('error'),

	activate: function(){

		var self = this;

		// Resolve a promise se já estiver resolvido
		if(this.get('position')){
			return Ember.RSVP.resolve(this.get('position'));
		}

		if(this.get('activatingPromise')){
			return this.get('activatingPromise');
		}

		var activatingPromise = new Ember.RSVP.Promise(function(resolve, reject){

			navigator.geolocation.getCurrentPosition(function(position){
				let lat = position.coords.latitude;
				let lng = position.coords.longitude;
				self.set('position', Ember.Object.create({ lat: lat, lng: lng, address: getAddressFromLocation(lat, lng) }));
				resolve(self.get('position'));
			}, function(error){
				self.set('position', null);

		 	  switch(error.code) {
					case error.PERMISSION_DENIED:
						self.set('error', "User denied the request for Geolocation.");
						break;
					case error.POSITION_UNAVAILABLE:
					 	self.set('error', "Location information is unavailable.");
					 	break;
					case error.TIMEOUT:
					 	self.set('error', "The request to get user location timed out.");
					 	break;
					case error.UNKNOWN_ERROR:
					 	self.set('error', "An unknown error occurred.");
					 	break;
				}

				reject(error);
			});
		});

		this.set('activatingPromise', activatingPromise);

		return activatingPromise;
	}
});
