import Ember from 'ember';
import DS from 'ember-data';
import pathFor from 'candidates-database/utils/path-for';
import QueryParamsObject from 'candidates-database/utils/query-params-object';

export default Ember.Service.extend({

  endpoint: "",
  rpp: 25,
  query: QueryParamsObject.create({}),
  filterQuery: QueryParamsObject.create({}),
  sort: Ember.A([]),
  userGeo: null,
  company: Ember.inject.service(),

  isSearching: Ember.computed.alias('searching'),
  isFiltering: Ember.computed.alias('filtering'),
  isSorting: Ember.computed.alias('sorting'),
  isChangingPage: Ember.computed.alias('changingPage'),

  setup: function(){
    var self = this;
    Ember.$('body').keypress(function(e){
      if(e.keyCode == 13){
        self.runSearch();
      }
    })
  }.on('init'),

  zeroResults: function(){
    return this.get('found') == 0;
  }.property('found'),

  searchParams: function(){
    return {
      query:         this.get('query').toParam(),
      filter_query:  this.get('filterQuery').toParam(),
      user_geo:      this.get('userGeo')
    }
  }.property().volatile(),

  _search: function(){
    var self = this;

    var postData = Ember.$.extend({  
      sort:          this.get('sort'),  
      rpp:           this.get('rpp'),
      page:          this.get('page'),
      company_id:    this.get('company.id')
    }, this.get('searchParams'));

    if(self.get('searchAjaxRequest')){
      self.get('searchAjaxRequest').abort();
    }

    var searchAjaxRequest = Ember.$.ajax({
      url: pathFor(self.get('endpoint')),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(postData)
    })

    self.set('searchAjaxRequest', searchAjaxRequest);

    return searchAjaxRequest;
  },

  pages: function(){
    var total = Math.ceil(this.get('found')/this.get('rpp'));
    return Ember.Object.create({ total: total, current: this.get('page') });
  }.property('page', 'rpp', 'found'),

  runSearch: function(){
    var self = this;
    this.set('facets', {});
    this.set('page', 1);
    this.set('filterQuery', QueryParamsObject.create({}));
    this.set('searching', true);
    return new Ember.RSVP.Promise(function(resolve, reject){
      self._search().then(function(response){
        self.set('searching', false);
        self.set('found', response.found);
        resolve(response);
      }, function(error){
        if(error.statusText !== "abort"){
          self.set('searching', false);
        }
        reject(error);
      });
    });
  },

  runFilter: function(){
    var self = this;
    this.set('page', 1);
    this.set('filtering', true);
    return new Ember.RSVP.Promise(function(resolve, reject){
      self._search().then(function(response){
        self.set('filtering', false);
        self.set('found', response.found);
        resolve(response.results);
      }, function(error){
        if(error.statusText !== "abort"){
          self.set('filtering', false);
        }
        reject(error);
      });
    });
  },

  runSort: function(){
    var self = this;
    this.set('page', 1);
    self.set('sorting', true);
    return new Ember.RSVP.Promise(function(resolve, reject){
      self._search().then(function(response){
        self.set('sorting', false);
        resolve(response.results);
      }, function(error){
        if(error.statusText !== "abort"){
          self.set('sorting', false);
        }
        reject(error);
      });
    });
  },

  changePage: function(page){
    var self = this;
    this.set('page', page);
    this.set('changingPage', true);
    return new Ember.RSVP.Promise(function(resolve, reject){
      self._search().then(function(response){
        self.set('changingPage', false);
        resolve(response.results);
      }, function(error){
        if(error.statusText !== "abort"){
          self.set('changingPage', false);
        }
        reject(error);
      });
    });
  },

  clearFilters: function(){
    this.set('filterQuery', QueryParamsObject.create({}));
  },

  clearResults: function(){
    this.set('found', 0);
  }
});
