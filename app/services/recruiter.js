import Ember from 'ember';

export default Ember.Service.extend({
	init: function(){
		this._super(...arguments);
		if (window.recruiterData) {
	    this.set('data', Ember.Object.create(window.recruiterData));
		} 
	},
	
	id: Ember.computed.alias('data.id'),
	name: Ember.computed.alias('data.name')
	
});
