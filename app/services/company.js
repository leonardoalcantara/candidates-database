import Ember from 'ember';

export default Ember.Service.extend({
	init: function(){
		this._super(...arguments);
		if (window.companyData) {
	    this.set('data', Ember.Object.create(window.companyData));
		} 
	},
	
	id: Ember.computed.alias('data.id'),
	name: Ember.computed.alias('data.name'),
	logo: Ember.computed.alias('data.logo'),
	addresses: Ember.computed.alias('data.addresses'),
	opportunities: Ember.computed.alias('data.opportunities')
	
});
