import Ember from 'ember';

export function telephone(text) {

	var DDD = ["11", "12", "13", "14", "15", "16", "17", "18", "19", "21", "22", "24", "27", "28", "31", "32", "33", "34", "35", "37", "38", "41", "42", "43", "44", "45", "46", "47", "48", "49", "51", "53", "54", "55", "61", "62", "63", "64", "65", "66", "67", "68", "69", "71", "73", "74", "75", "77", "79", "81", "82", "83", "84", "85", "86", "87", "88", "89", "91", "92", "93", "94", "95", "96", "97", "98", "99"];

	function mask(format, first){
		var dddFormat = format.substr(0,2);
		if((format.substr(2, format.length).length)/first < 2){
			var second = 7;
		} else {
			var second = 6;
		}
		if(DDD.indexOf(dddFormat) >= 0){
			final += "("+dddFormat+") " + format.substr(2,first) + "-" + format.substr(second,4);
		}
	}

  function maskWithoutDDD(format, first){
    final += format.substr(0,first) + "-" + format.substr((first),4);
  }

	function withCountry(format, first){
		var eq = format.length - first - 6;
		var ddiFormat = format.substr(0,eq);
		format = format.substr(eq,format.length);
		final += "+"+ddiFormat+" ";
		mask(format, first);
	}

	if(text) {
  	var format = text.replace(/\D/g, '');
  	var final = '';
  	switch(format.length) {
  		case 0:
  			final = '';
  			break;
      case 8:
        maskWithoutDDD(format, 4);
        break;
      case 9:
        maskWithoutDDD(format, 5);
        break;
      case 10:
        mask(format, 4);
        break;
  		case 11:
  			if(format.substr(0,1) == "0"){
  				format = format.substr(1,format.length);
  				mask(format, 4);
				} else {
					mask(format, 5);
				}
  			break;
  		case 12:
  			if(format.substr(0,1) == "0"){
  				format = format.substr(1,format.length);
  				mask(format, 5);
  			} else {
  				withCountry(format, 4);
  			}
  			break;
  		case 13:
  			var verifyCellphone = format.slice(-9).substr(0,1);
  			if(verifyCellphone == "9"){
  				withCountry(format, 5);
  			} else {
  				withCountry(format, 4);
  			}
  			break;
  		default:
  			final = text;
  			break;
  	}
		return final;
	}
};

export default Ember.Handlebars.makeBoundHelper(telephone);