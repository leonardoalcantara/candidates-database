import Ember from 'ember';
import { translationMacro as t } from "ember-i18n";

export default Ember.Helper.extend({
	i18n: Ember.inject.service(),
  compute(params, hash) {
		var parts = params[0].split('|');
		if(parts.length == 1) {
			return parts[0];
		} else if(parts.length == 2){
			return parts[0] + ' - ' + this.get('i18n').t('language_levels.'+parts[1]);
		}
  }
});