import Ember from 'ember';

export default Ember.Helper.extend({
	i18n: Ember.inject.service(),
  compute(params, hash) {
		var parts = /^\[([*0-9.]*) TO ([*0-9.]*)\]$/.exec(params[0])
		if(parts.length == 3){
			if(parts[1] == "*"){
				return this.get('i18n').t('salary_range.to', { to: parts[2] });
			} else if(parts[2] == "*") {
				return this.get('i18n').t('salary_range.from', { from: parts[1] });
			} else {
				return this.get('i18n').t('salary_range.from_to', { from: parts[1], to: parts[2] });
			}
		} else {
			return "";
		}
  }
});