import Ember from 'ember';

export function nl2br(text) {
  var breakTag = '<br />';
	return new Ember.Handlebars.SafeString((text.trim() + '')
  	.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2'));
};

export default Ember.Handlebars.makeBoundHelper(nl2br);