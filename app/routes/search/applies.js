import Ember from 'ember';

export default Ember.Route.extend({
  setupController: function(controller, model){
    controller.set('modelRoot', this.modelFor('application'));
  }
});
