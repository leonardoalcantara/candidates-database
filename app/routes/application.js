import Ember from 'ember';

export default Ember.Route.extend({
  model: function(){
    return Ember.Object.create({
      term:""
    })
  },
	
  setupController: function(controller, model){
    controller.set('model', model);
  },
	
	actions: {
		openModal: function(template, model){
			console.log("OPEN MODAL", template, model)
			var model = model || {};
			this.render(template, {
	      into: 'application',
	      outlet: 'modal',
				model: model
	    })
	    Ember.run.scheduleOnce('afterRender', function(){
	    	Ember.$("html, body").addClass('noScroll');
	    })
		},
	
		closeModal: function(){
			this.disconnectOutlet({
	      parentView: 'application',
	      outlet: 'modal'
	    });
	    Ember.run.scheduleOnce('afterRender', function(){
	    	Ember.$("html, body").removeClass('noScroll');
	    })
		}
	}
});
